package com.kenmr.androidbasics;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class ServicesActivity extends AppCompatActivity {
    private final static String TAG = ServicesActivity.class.getName();
    private ServiceConnection mServiceConnection;
    private boolean mIsBound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
        Button btnStartService = findViewById(R.id.btn_services_start_service);
        btnStartService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ServicesActivity.this, ShowNoticeService.class);
                startService(intent);
            }
        });
        Button btnStopService = findViewById(R.id.btn_services_stop_service);
        btnStopService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ServicesActivity.this, ShowNoticeService.class);
                stopService(intent);
            }
        });
        Button btnBindService = findViewById(R.id.btn_services_bind_service);
        btnBindService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ServicesActivity.this, ShowNoticeService.class);
                /**
                 * Callbacks for service binding, passed to bindService()
                 */
                mServiceConnection = new ServiceConnection() {
                    @Override
                    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                        Log.d(TAG, "The onServiceConnected() event");
                        ShowNoticeService.NoticeBinder noticeBinder = (ShowNoticeService.NoticeBinder) iBinder;
                        ((ShowNoticeService.NoticeBinder) iBinder).getService().callStartTimer();
                        mIsBound = true;
                    }

                    @Override
                    public void onServiceDisconnected(ComponentName componentName) {
                        Log.d(TAG, "The onServiceDisconnected() event");
                        mIsBound = false;
                    }
                };
                bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
            }
        });
        Button btnUnbindService = findViewById(R.id.btn_services_unbind_service);
        btnUnbindService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mIsBound) {
                    unbindService(mServiceConnection);
                    mIsBound = false;
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mIsBound) {
            unbindService(mServiceConnection);
            mIsBound = false;
        }
    }
}
