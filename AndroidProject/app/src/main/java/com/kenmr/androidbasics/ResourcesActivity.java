package com.kenmr.androidbasics;

import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class ResourcesActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView mImgChangePictureEffect;
    private TextView mTvBooksInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resources);
        mImgChangePictureEffect = findViewById(R.id.img_resources_show_change_picture_effect);
        mTvBooksInfo = findViewById(R.id.tv_resources_show_books_info);
        Button btnChangePicture = findViewById(R.id.btn_resources_change_picture);
        btnChangePicture.setOnClickListener(this);
        Button btnChangeColor = findViewById(R.id.btn_resources_change_color);
        btnChangeColor.setOnClickListener(this);
        Button btnLoadXML = findViewById(R.id.btn_resources_load_xml);
        btnLoadXML.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_resources_change_picture:
                mImgChangePictureEffect.setImageResource(R.drawable.resources_using_resource_example_1);
                break;
            case R.id.btn_resources_change_color:
                mImgChangePictureEffect.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                break;
            case R.id.btn_resources_load_xml:
                XmlResourceParser bookParser = getResources().getXml(R.xml.books);
                StringBuffer books = new StringBuffer();
                try {
                    int eventType = bookParser.next();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.TEXT) {
                            if (TextUtils.equals(bookParser.getName(), "Text")) {
                                books.append(" - ");
                            }
                            books.append(bookParser.getText());
                        } else if (eventType == XmlPullParser.START_TAG && TextUtils.equals(bookParser.getName(), "Chapter")) {
                            books.append("\n");
                        }
                        eventType = bookParser.next();
                    }
                } catch (XmlPullParserException | IOException e) {
                    e.printStackTrace();
                }
                mTvBooksInfo.setText(books);
                break;
        }
    }
}
