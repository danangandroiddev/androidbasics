package com.kenmr.androidbasics;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onMenuClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.main_open_resources_activity:
                intent = new Intent(MainActivity.this, ResourcesActivity.class);
                startActivity(intent);
                break;
            case R.id.main_open_activity_lifecycle:
                intent = new Intent(MainActivity.this, LifeCycleActivity.class);
                startActivity(intent);
                break;
            case R.id.main_open_activity_service:
                intent = new Intent(MainActivity.this, ServicesActivity.class);
                startActivity(intent);
                break;
        }
    }
}
