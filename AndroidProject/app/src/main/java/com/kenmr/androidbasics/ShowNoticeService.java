package com.kenmr.androidbasics;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class ShowNoticeService extends Service {
    private final static String TAG = ShowNoticeService.class.getName();
    private final static int REPEAT_TIME = 3000; // Delay time
    private Timer mTimerNotice;
    private TimerTask mTimerTaskShowNoticeToast;

    public ShowNoticeService() {
    }

    /**
     * Called when the service is being created.
     */
    @Override
    public void onCreate() {
        Log.d(TAG, "The onCreate() event");
    }

    /**
     * The service is starting, due to a call to startService()
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "The onStartCommand() event");
        startTimer();
        return START_STICKY; //try with other mode
    }

    /**
     * A client is binding to the service with bindService()
     */
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "The onBind() event");
        NoticeBinder binder = new NoticeBinder();
        return binder;
    }

    /**
     * Called when all clients have unbound with unbindService()
     */
    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "The onUnbind() event");
        stopTimer();
        return false;//true: allow rebind, false: not rebind
    }

    /**
     * Called when a client is binding to the service with bindService()
     */
    @Override
    public void onRebind(Intent intent) {
        Log.d(TAG, "The onRebind() event");
        startTimer();
    }

    /**
     * Called when The service is no longer used and is being destroyed
     */
    @Override
    public void onDestroy() {
        Log.d(TAG, "The onDestroy() event");
        stopTimer();
    }

    private void startTimer() {
        mTimerNotice = new Timer();
        mTimerTaskShowNoticeToast = new TimerTask() {
            @Override
            public void run() {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        int tid = android.os.Process.myTid();
                        int pid = android.os.Process.myPid();
                        Toast.makeText(ShowNoticeService.this, "Notice Service Push Tid " + tid + " Pid " + pid, Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Notice Service Push event " + tid + " Pid " + pid);
                    }
                });
            }
        };
        mTimerNotice.schedule(mTimerTaskShowNoticeToast, 0, REPEAT_TIME);
    }

    public void callStartTimer() {
        startTimer();
    }

    private void stopTimer() {
        if (mTimerNotice != null) {
            mTimerNotice.cancel();
            mTimerNotice = null;
        }
    }

    public class NoticeBinder extends Binder {
        ShowNoticeService getService() {
            return ShowNoticeService.this;
        }
    }
}
